# Universal-DB
Universal-DB Demo (VLDB 2015)

universal-db.herokuapp.com

Yodsawalai Chodpathumwan, Amirhossein Aleyasen, Arash Termehchy, Yizhou Sun, “Universal-DB: Towards Representation Independent Graph Analytics” (http://web.engr.oregonstate.edu/~termehca/universal-vldb15.pdf)


## Requirement
 - PHP
 - Apache Server
 
For Windows, you can install XAMPP (https://www.apachefriends.org/index.html) that includes all the above (+MySQL+Perl), for OS X install MAMP (https://www.mamp.info/en/)

## Installation
 - Clone the project on the htdocs directory (in your xampp/mamp installation).
 - Start Apache Service
 - Check out http://localhost/Universal-DB/
